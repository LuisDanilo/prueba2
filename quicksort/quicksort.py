# Python v3.7.2
# Algoritmo de ordenamiento Quicksort
def __quicksort(sub_a):
	'''
		Método recursivo del algoritmo.
		:sub_a: Sub arreglo al cual se le aplica el ordenamiento.
		:left_a: Arreglo con los valores menores al pivote
		:right_a: Arreglo con los valores mayores o iguales al pivote
		NOTA: Ya que python usa parámetros por referencia no fue necesario
		      hacer que la función recursiva tuviese un retorno.
	'''
	left_a = []
	right_a = []
	if (len(sub_a) > 1):
		# Aun se puede aplicar recursividad.
		pivot = sub_a[0]
		for cur_index in range(1,len(sub_a)):
			if (sub_a[cur_index] < pivot):
				left_a.append(sub_a[cur_index])
			else:
				right_a.append(sub_a[cur_index])
		sub_a.clear()
		__quicksort(left_a)
		__quicksort(right_a)
		sub_a.extend(left_a)
		sub_a.append(pivot)
		sub_a.extend(right_a)

def quicksort(a):
	'''
		Método principal del algoritmo.
		:a: Arreglo al cual se le aplica el ordenamiento.
	'''
	__quicksort(a)

if __name__ == "__main__":
	array = [5, 2, 7, 3, 1, 8, 2, 6, 9]
	print("BEFORE: ", array)
	quicksort(array)
	print("AFTER:  ", array)